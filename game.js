let MAX_HIGH_SCORES = 5;

var MyGame = {
    screens: {},
    cancelNextFrame: false,

    persistence: (function () {
        'use strict';
        var highScores = [],
            previousScores = localStorage.getItem('Breakum.highScores');
        
        // If there are previous scores
        if (previousScores !== null) {
            highScores = JSON.parse(previousScores);
        }

        function add(name, score) {
            let inserted = false;
            for (let i = 0; i < highScores.length; i++) {
                if (score > highScores[i].score) {
                    highScores.splice(i, 0, { name: name, score: score } );
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                highScores[highScores.length] = { name: name, score:score };
            }

            if (highScores.length > MAX_HIGH_SCORES) {
                console.log("Removing extras");
                highScores.splice(MAX_HIGH_SCORES-1,highScores.length-MAX_HIGH_SCORES);
            }

            localStorage['Breakum.highScores'] = JSON.stringify(highScores);
        }

        function remove(key) {
            delete highScores[key];
            localStorage['Breakum.highScores'] = JSON.stringify(highScores);
        }

        function reset() {
            highScores.length = 0;
            localStorage['Breakum.highScores'] = JSON.stringify(highScores);
        }

        function report() {
            var scores;

            scores = '';
            for (let i = 0; i < highScores.length; i++) {
                scores += '<h3>' + highScores[i].name + ': ' + highScores[i].score + '</h3>';
            }
            return scores;
        }

        return {
            add: add,
            remove: remove,
            reset: reset,
            report: report
        };
    }())
};

MyGame.game = (function(screens) {
    'use strict';

    function showScreen(screenId) {
        let screen = 0;
        let active = null;

        active = document.getElementsByClassName('active');
        for (screen = 0; screen < active.length; screen++) {
            active[screen].classList.remove('active');
        }

        screens[screenId].run();

        document.getElementById(screenId).classList.add('active');
    }

    function initialize() {
        let screen = null;

        for (screen in screens) {
            if (screens.hasOwnProperty(screen)) {
                screens[screen].initialize();
            }
        }

        showScreen('mainMenu');
    }

    return {
        initialize: initialize,
        showScreen: showScreen
    };
}(MyGame.screens));