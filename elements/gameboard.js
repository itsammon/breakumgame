'use strict';

let Gameboard = function () {

    let boardWidth = 1000;
    let boardHeight = 800;
    let numBlocksRow = 16;
    let numRows = 8;
    let TOP_ROW_OFFSET = 150;
    let BLOCK_HEIGHT = 30;
    let START_SPEED = 200;
    let currSpeed = START_SPEED;
    let SPEED_UP_AMOUNT = 50;
    let nextReleasedBall = 100;
    let blocksRemaining = 0;
    let BALL_SCREEN_RATIO = 0.035;
    let INITIAL_PADDLE_WIDTH = 125;
    let PADDLE_HEIGHT = 25;

    var that = {
        gameOver: false,
        ballMoving: false,
        boardWidth: boardWidth,
        boardHeight: boardHeight,
        countdownTime: 3000,
        blocks: null,
        numBlocks: [],
        blockRows: [],
        paddle: null,
        balls: [],
        input: null,
        numPaddles: 3,
        score: 0,
        numBlocksRemoved: 0,
        removedBlock: null,
    }

    function speedUpBalls() {
        console.log('Speeding up balls');
        currSpeed += SPEED_UP_AMOUNT;
        for (let i = 0; i < that.balls.length; i++) {
            that.balls[i].speed = currSpeed;
        }
    }

    function removeBlock(block) {
        block.alive = false;
        that.numBlocksRemoved++;
        blocksRemaining--;
        that.score += block.value;
        if (that.numBlocksRemoved === 4 || that.numBlocksRemoved === 12 
            || that.numBlocksRemoved === 36 || that.numBlocksRemoved === 62) {
            speedUpBalls();
        }
        that.removedBlock = { x: block.location.x + block.size.w/2, y: block.location.y + block.size.h/2, w: block.size.w, h: block.size.h };
    }

    function topRowHit() {
        if (that.paddle.isShrunk === false) {
            that.paddle.isShrunk = true;
            that.paddle.reduceWidth();
        }
    }

    function intersectRect(r1, r2) {
        return !(r2.location.x > (r1.location.x+r1.size.w) ||
           (r2.location.x+r2.size.w) < r1.location.x ||
           r2.location.y > r1.location.y+r1.size.h ||
           r2.location.y+r2.size.h < r1.location.y);
    }

    function checkBlocks(ball) {
        for (let row = 0; row < that.blockRows.length; row++) {
            if (intersectRect(ball, that.blockRows[row])) {
                for (let column = 0; column < that.blocks[row].length; column++) {
                    if (that.blocks[row][column].alive && intersectRect(ball, that.blocks[row][column])) {
                        removeBlock(that.blocks[row][column]);
                        that.numBlocks[row]--;
                        if (that.numBlocks[row] == 0) {
                            that.score += 25;
                        }

                        if (that.score >= nextReleasedBall) {
                            that.balls.push(createBall());
                            nextReleasedBall += 100;
                        }

                        // Handle hits to the top row
                        if (row == 0) {
                            topRowHit();
                        }

                        /*
                        if (ball.location.x < that.blocks[row][column].location.x + that.blocks[row][column].w) {
                            return 'r'; // Hit right of brick
                        }
                        if (ball.location.x > that.blocks[row][column].location.x) {
                            return 'l'; // Hit left of brick
                        }
                        if (ball.location.y < that.blocks[row][column].location.y+(that.blocks[row][column].h)) {
                            return 'b'; // Hit bottom of brick
                        }
                        if (ball.location.y > that.blocks[row][column].location.y) {
                            return 't'; // Hit top of brick
                        }
                        */
                        return 'h';
                    }
                }
            }
        }
        return null;
    }

    function Ball(spec) {
        let that = {
            location: { x: spec.location.x, y: spec.location.y },
            size: { w: spec.size.w, h: spec.size.h },
            image: spec.image,
            maxSpeed: spec.maxSpeed,
            //speedIncrement: 2,
            speed: spec.speed,
            direction: { x: spec.direction.x, y: spec.direction.y },

            // Update the position of the ball, needs the paddle for collision detection
            // Returns true if the ball goes out of bounds
            // Returns false otherwise
            update: function (elapsedTime, paddle) {
                that.location.x += that.speed * that.direction.x * (elapsedTime/1000);
                that.location.y += that.speed * that.direction.y * (elapsedTime/1000);

                if (that.location.x < 0) {
                    that.reverseX();
                    that.location.x = 0;
                }

                if (that.location.x + that.size.w > boardWidth) {
                    that.reverseX();
                    that.location.x = boardWidth - (that.size.w);
                }

                if (that.location.y < 50) {
                    that.reverseY();
                    that.location.y = 50;
                }

                if (that.location.y > boardHeight) {
                    // Handle paddle loss and reset board state
                    return true;
                }

                let hitBlock = checkBlocks(that);
                if (hitBlock != null) {
                    that.reverseY();
                    /*if (hitBlock === 't' || hitBlock === 'b') {
                        that.reverseY();
                    }
                    if (hitBlock === 'r' || hitBlock === 'l') {
                        that.reverseX();
                    }*/
                }

                // Paddle Collision
                if (that.speed > 0 && intersectRect(that, paddle)) {
                    that.direction.y = -that.direction.y;
                    that.location.y = paddle.location.y - that.size.h;

                    that.direction.x = ((that.location.x + that.size.w/2) - (paddle.location.x + paddle.size.w/2)) /
                                    (paddle.size.w/2);
                    let directionLength = (Math.sqrt(Math.pow(that.direction.x, 2) + Math.pow(that.direction.y, 2)));
                    that.direction.x = that.direction.x / directionLength;
                    that.direction.y = that.direction.y / directionLength;

                    // Speedup is due to brick removal
                    //that.speed += that.speedIncrement; 
                    that.speed = Math.min(that.speed, that.maxSpeed);
                }


                if (blocksRemaining <= 0) {
                    gameOver()
                    that.ballMoving = false;
                }

                // No need to reset the board
                return false;
            },

            reverseX: function() {
                that.direction.x = -(that.direction.x);
            },
            reverseY: function() {
                that.direction.y = -(that.direction.y);
            }
        }
        
        return that;
    }

    // Generator function for blocks
    function Block(spec) {
        var that = {
            location: { x: spec.x, y: spec.y },
            size: { w: spec.w, h: spec.h },
            color: spec.color,
            value: spec.value,
            alive: spec.alive
        }

        return that;
    }

    // Generator function for paddles
    function Paddle(spec) {
        var that = {
            isShrunk: false,
            location: spec.location,
            size: spec.size,
            color: spec.color,
            moveSpeed: 500, // In pixels per second

            moveRight: function (elapsedTime) {
                // Move the paddle to the right
                spec.location.x += that.moveSpeed * elapsedTime / 1000;
                if (spec.location.x + spec.size.w > boardWidth) {
                    spec.location.x = boardWidth - spec.size.w;
                }
            },

            moveLeft: function (elapsedTime) {
                // Move the paddle to the right
                spec.location.x -= that.moveSpeed * elapsedTime / 1000;
                if (spec.location.x < 0) {
                    spec.location.x = 0;
                }
            },

            reduceWidth: function () {
                spec.location.x - (spec.size.w/4); // Shift the edge of the paddle for less jarring change.
                spec.size.w = spec.size.w / 2;
            }
        }

        return that;
    }

    // Create the blocks in the gameboard
    function createBlocks() {
        console.log("Creating blocks");
        let blocks = [];
        for (let row = 0; row < numRows; row++) {
            that.numBlocks[row] = numBlocksRow;
            blocks[row] = [];
            that.blockRows[row] = {
                location: { x: 0, y: BLOCK_HEIGHT*row+TOP_ROW_OFFSET },
                size: { w: boardWidth, h: BLOCK_HEIGHT }
            };
            for (let column = 0; column < numBlocksRow; column++) {
                blocks[row][column] = Block({
                    x: (boardWidth/numBlocksRow)*column, y: BLOCK_HEIGHT*row + TOP_ROW_OFFSET,
                    w: (boardWidth/numBlocksRow), h: BLOCK_HEIGHT,
                    color: '', value: 1, alive: true,
                })
                if (row < 2) {
                    blocks[row][column].color = 'rgba(0, 255, 0, 1)'; // Green
                    blocks[row][column].value = 5;
                }
                else if (row < 4) {
                    blocks[row][column].color = 'rgba(0, 0, 255, 1)'; // Blue
                    blocks[row][column].value = 3;
                }
                else if (row < 6) {
                    blocks[row][column].color = 'rgba(255, 165, 0, 1)'; // Orange
                    blocks[row][column].value = 2;
                }
                else {
                    blocks[row][column].color = 'rgba(255, 255, 0, 1)'; // Yellow
                    blocks[row][column].value = 1;
                }
            }
        }
        blocksRemaining = numBlocksRow*numRows;
        return blocks;
    }

    function createPaddle() {
        console.log('Creating paddle');
        let paddle = Paddle({
        location: { x: boardWidth/2-(INITIAL_PADDLE_WIDTH/2), y: boardHeight-PADDLE_HEIGHT*2 },
            size: { w: INITIAL_PADDLE_WIDTH, h: PADDLE_HEIGHT },
            color: 'rgba(128, 128, 128,1)'
        });

        that.input.registerCommand(KeyEvent.DOM_VK_A, paddle.moveLeft);
        that.input.registerCommand(KeyEvent.DOM_VK_D, paddle.moveRight);
        that.input.registerCommand(KeyEvent.DOM_VK_J, paddle.moveLeft);
        that.input.registerCommand(KeyEvent.DOM_VK_L, paddle.moveRight);
        that.input.registerCommand(KeyEvent.DOM_VK_LEFT, paddle.moveLeft);
        that.input.registerCommand(KeyEvent.DOM_VK_RIGHT, paddle.moveRight);

        return paddle;
    }

    function createBall() {
        console.log('Creating ball');
        let ball = Ball({
            location: { x: boardWidth/2-(boardWidth*(BALL_SCREEN_RATIO/2)), y: boardHeight-100 },
            size: { w: boardWidth*BALL_SCREEN_RATIO, h: boardWidth*BALL_SCREEN_RATIO },
            maxSpeed: 300, // Not currently used
            speed: currSpeed,
            direction: { x: 3/5, y: -4/5 }
        });
        return ball;
    }

    function gameOver() {
        that.gameOver = true;
        // Add high score if it is high enough
        name = window.prompt('Please enter your name:', 'John Doe');
        if (name != 'null') {
            MyGame.persistence.add(name, that.score);
        }
    }

    that.update = function(elapsedTime) {
        if (that.ballMoving) {
            let toRemove = [];
            for (let index = 0; index < that.balls.length; index++) {
                if (that.balls[index].update(elapsedTime, that.paddle)) {
                    toRemove.push(index);
                }
            }
            for (let i = 0; i < toRemove.length; i++) {
                that.balls.splice(toRemove[i], 1);
            }
            if (that.balls.length <= 0) {
                that.ballOut();
            }
        } else {
            if (!that.gameOver) {
                // Handle the countdown before the game
                that.countdownTime -= elapsedTime;
                if (that.countdownTime <= 0) {
                    that.ballMoving = true;
                    that.countdownTime = 3000;
                }
            }
        }
    }

    that.ballOut = function() {
        // Reset the number of Blocks removed
        that.numBlocksRemoved = 0;
        that.numPaddles--;
        currSpeed = START_SPEED;
        if (that.numPaddles < 0) {
            gameOver();
        } else {
            that.paddle = createPaddle();
            that.balls.length = 0; // Reset balls
            that.balls.push(createBall());
        }
        that.ballMoving = false;
    }

    that.newGame = function() {
        currSpeed = START_SPEED;
        nextReleasedBall = 100;
        blocksRemaining = 0;
        that.countdownTime = 3000,
        that.numPaddles = 3;
        that.score = 0;
        that.numBlocksRemoved = 0;
        that.blocks = createBlocks();
        that.paddle = createPaddle();
        that.balls.length = 0;
        that.balls.push(createBall());
        that.ballMoving = false;
        that.gameOver = false;
    }

    that.initialize = function(input) {
        that.input = input;
    }

    return that;
}

