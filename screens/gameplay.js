'use strict';

MyGame.screens['gameplay'] = (function(input) {
    let previousTime = performance.now();
    let elapsedTime = 0;
    let myKeyboard = null;
    let myGameboard = null;
    let myPaddle = null;
    let paused = true;

    function toInGameMenu() {
        paused = true;

        MyGame.game.showScreen('inGameMenu');
    }

    function handleInput(elapsedTime) {
        myKeyboard.update(elapsedTime);
    }

    function update(elapsedTime) {
        myGameboard.update(elapsedTime);
        Renderer.update(elapsedTime);
    }

    function render(elapsedTime) {
        Graphics.beginRender();
        Renderer.render(elapsedTime);
    }

    function gameLoop(time) {
        elapsedTime = time - previousTime;
        previousTime = time;
        handleInput(elapsedTime);

        if (!paused) {
            update(elapsedTime);
            render(elapsedTime);

            if (myGameboard.gameOver) {
                paused = true;
            }
        }
        
        requestAnimationFrame(gameLoop);
    }

    function newGame() {
        myGameboard.newGame();
    }

    function run() {
        paused = false;

    }

    function initialize() {
        console.log('Initializing gameplay screen');
        myGameboard = Gameboard();
        myPaddle = myGameboard.paddle;
        myKeyboard = input.Keyboard();

        myGameboard.initialize(myKeyboard);

        Renderer.initialize(myGameboard);

        myKeyboard.registerCommand(KeyEvent.DOM_VK_ESCAPE, toInGameMenu);

        requestAnimationFrame(gameLoop);
    }

    return {
        newGame: newGame,
        initialize: initialize,
        run: run
    };
}(MyGame.input));