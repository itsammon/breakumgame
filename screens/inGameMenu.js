MyGame.screens['inGameMenu'] = (function(game) {
    'use strict';

    function run() {
        // Intentionally blank
    }

    function initialize() {

		document.getElementById('resume-btn').addEventListener(
			'click',
            function() {
                game.showScreen('gameplay'); });
		
		document.getElementById('exit-btn').addEventListener(
			'click',
            function() {
                game.showScreen('mainMenu'); });
    }

    return {
        initialize: initialize,
        run: run
    };
}(MyGame.game));