MyGame.screens['highscores'] = (function(game) {
    'use strict';

    let myKeyboard = null;

    function run() {
        // Need to get the high scores into the screens
        var highScoresDiv = document.getElementById('scores');

        highScoresDiv.innerHTML = '<h1>High Scores</h1>';
        highScoresDiv.innerHTML += MyGame.persistence.report();
    }

    function initialize() {
		document.getElementById('h-back-btn').addEventListener(
			'click',
            function() {
                game.showScreen('mainMenu'); });

		document.getElementById('h-reset-btn').addEventListener(
			'click',
            function() {
                MyGame.persistence.reset();
                game.showScreen('highscores'); });
    }

    return {
        initialize: initialize,
        run: run
    };
}(MyGame.game));