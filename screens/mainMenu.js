MyGame.screens['mainMenu'] = (function(game) {
    'use strict';

    function run() {
        // Intentionally blank
    }

    function initialize() {
		document.getElementById('newGame-btn').addEventListener(
			'click',
            function() {MyGame.screens['gameplay'].newGame();
                game.showScreen('gameplay'); });
		
		document.getElementById('highScores-btn').addEventListener(
			'click',
            function() {
                game.showScreen('highscores'); });
		
		document.getElementById('credits-btn').addEventListener(
			'click',
            function() {
                game.showScreen('credits'); });
    }

    return {
        initialize: initialize,
        run: run
    };
}(MyGame.game));