'use strict';

let NUM_PARTICLES = 50;

let Renderer = (function() {
    let myGameboard = null;
    let myBallTexture = null;
    let particles = null;

    let myBackground = Graphics.Rectangle({
            corner: { x: 0, y: 0 },
            size: { w: Graphics.getCanvasWidth(), h: Graphics.getCanvasHeight() },
            rotation: 0,
            fillStyle: 'rgba(255, 0, 0, 0.5)',
            strokeStyle: 'rgba(0, 0, 0, 0)'
          });

    function drawBlocks() {
        let blocks = myGameboard.blocks;
        for (let row = 0; row < blocks.length; row++) {
            for (let column = 0; column < blocks[row].length; column++) {
                if (blocks[row][column].alive) {
                    let myBlock = Graphics.Rectangle({
                        corner: { x: blocks[row][column].location.x, y: blocks[row][column].location.y },
                        size: { w: blocks[row][column].size.w, h: blocks[row][column].size.h },
                        rotation: 0,
                        fillStyle: blocks[row][column].color,
                        strokeStyle: 'rgba(0, 0, 0 , 1)'
                    });
                    myBlock.draw();
                }
            }
        }
    }

    function drawPaddle() {
        let paddle = myGameboard.paddle;
        let myPaddle = Graphics.Rectangle({
            corner: { x: paddle.location.x, y: paddle.location.y },
            size: { w: paddle.size.w, h: paddle.size.h },
            rotation: 0,
            fillStyle: paddle.color,
            strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        myPaddle.draw();
    }

    function drawBalls() {
        let balls = myGameboard.balls;
        for (let index = 0; index < balls.length; index++) {
            myBallTexture.changeCenter({
                x: balls[index].location.x + (balls[index].size.w /2),
                y: balls[index].location.y + (balls[index].size.h /2) });
            myBallTexture.changeSize(balls[index].size.w, balls[index].size.h);
            myBallTexture.draw();
        }
    }

    function drawScore() {
        var myText = Graphics.Text({
            text: 'Score: ' + myGameboard.score,
            font: '20px Arial, sans-serif',
            fill: 'rgba(238, 238, 238,1)',
            stroke: 'rgba(238, 238, 238,1)',
            location: {x: myGameboard.boardWidth-100, y: myGameboard.boardHeight-25 },
            rotation: 0
        });

        myText.draw();
    }

    function drawExtraPaddles() {
        let myEdge = Graphics.Rectangle({
            corner: { x: 0, y: 50 },
            size: { w: Graphics.getCanvasWidth(), h: 2 },
            rotation: 0,
            fillStyle: 'rgba(30, 144, 255, 1)',
            strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        myEdge.draw();

        for (let i = 0; i < myGameboard.numPaddles; i++) {
            let myExtraPaddle = Graphics.Rectangle({
                corner: { x: Graphics.getCanvasWidth() - 125*(i+1), // Starts at leftmost edge with drawing
                     y: 10 },
                size: { w: 100, h: myGameboard.paddle.size.h },
                rotation: 0,
                fillStyle: myGameboard.paddle.color,
                strokeStyle: 'rgba(0, 0, 0, 1)'
            });
            myExtraPaddle.draw();
        }
    }

    function drawCountdown() {
        var myText = Graphics.Text({
            text: Math.floor(myGameboard.countdownTime/1000+1),
            font: '100px Arial, sans-serif',
            fill: 'rgba(255,0,0,1)',
            stroke: 'rgba(0,0,0,1)',
            location: {x: myGameboard.boardWidth/2, y: myGameboard.boardHeight/2 },
            rotation: 0
        });

        myText.location = { x: myGameboard.boardWidth/2-myText.width/2, y: myGameboard.boardHeight/2-myText.height/2 };

        myText.draw();
    }

    function drawGameOver() {
        // If they still have paddles, then they won
        if( myGameboard.numPaddles > 0) {
            var myText = Graphics.Text({
                text: 'You Won!',
                font: '100px Arial, sans-serif',
                fill: 'rgba(255,0,0,1)',
                stroke: 'rgba(0,0,0,1)',
                location: {x: myGameboard.boardWidth/2, y: myGameboard.boardHeight/2 },
                rotation: 0
            });

            myText.draw();
        } else {
            var myText = Graphics.Text({
                text: 'Game Over!',
                font: '100px Arial, sans-serif',
                fill: 'rgba(255,0,0,1)',
                stroke: 'rgba(0,0,0,1)',
                location: {x: myGameboard.boardWidth/2, y: myGameboard.boardHeight/2 },
                rotation: 0
            });

            myText.draw();
        }
    }

    function update(elapsedTime) {
        particles.update(elapsedTime);

        if (myGameboard.removedBlock !== null) {
            // Move the particle system to a new location
            particles.changeCenter({ x: myGameboard.removedBlock.x, y: myGameboard.removedBlock.y });
            // Create a collection of particles
            for (let i = 0; i < NUM_PARTICLES; i++) {
                particles.create();
            }
            myGameboard.removedBlock = null;
        }
    };

    function render(elapsedTime) {
        Graphics.beginRender();
        drawBlocks();
        drawPaddle();
        drawBalls();
        drawScore();
        drawExtraPaddles();
        particles.render(elapsedTime);
        if (!myGameboard.ballMoving) {
            if (myGameboard.gameOver) {
                drawGameOver();
            } else {
                drawCountdown();
            }
        }
    };

    function initialize(gameboard) {
        // Setup the graphics layer
        Graphics.initialize();

        // Image for ball in game
        myBallTexture = Graphics.Texture({
            imageSource: 'images/moon1.png',
            center: { x: 0,
                 y: 0 },
            width: 50,
            height: 50,
            rotation: 0,
        });

        // Particle System for brick impacts
	    particles = ParticleSystem( {
			image : 'images/fire.png',
			center: {x: 300, y: 300},
			speed: {mean: 50, stdev: 25},
			lifetime: {mean: 1, stdev: .25} },
		    Graphics);


        myGameboard = gameboard;
    }

    return {
        update: update,
        render: render,
        initialize: initialize
    }
}());