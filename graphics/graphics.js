'use strict';

let Graphics = (function () {
    let context = null;

    function initialize() {
        let canvas = document.getElementById('canvas');
        context = canvas.getContext('2d');

        CanvasRenderingContext2D.prototype.clear = function() {
            this.save();
            this.setTransform(1,0,0,1,0,0);
            this.clearRect(0,0, canvas.width, canvas.height);
            this.restore();
        };
    }

    function getCanvasWidth() {
        return canvas.width;
    }

    function getCanvasHeight() {
        return canvas.height;
    }

    function Rectangle(spec) {
        let that = {};

        that.draw = function() {
            context.save();

            context.translate(spec.corner.x + spec.size.w / 2, spec.corner.y + spec.size.h / 2);
            context.rotate(spec.rotation);
            context.translate(-(spec.corner.x + spec.size.w / 2), -(spec.corner.y + spec.size.h / 2));

            context.fillStyle = spec.fillStyle;
            context.fillRect(spec.corner.x, spec.corner.y, spec.size.w, spec.size.h);

            context.strokeStyle = spec.strokeStyle;
            context.strokeRect(spec.corner.x, spec.corner.y, spec.size.w, spec.size.h);

            context.restore();
        }

        return that;
    }

    function Texture(spec) {
        var that = {},
            ready = false,
            image = new Image();

        image.onload = () => {
            ready = true;
        };
        image.src = spec.imageSource;

        that.changeCenter = function(newCenter) {
            spec.center.x = newCenter.x;
            spec.center.y = newCenter.y;
        }

        that.changeSize = function(newWidth, newHeight) {
            spec.width = newWidth;
            spec.height = newHeight;
        }

        that.draw = function() {
            if (ready) {
                context.save();

                context.translate(spec.center.x, spec.center.y);
                context.rotate(spec.rotation);
                context.translate(-spec.center.x, -spec.center.y);

                context.drawImage(
                    image,
                    spec.center.x - spec.width / 2,
                    spec.center.y - spec.height / 2,
                    spec.width, spec.height);

                context.restore();
            }
        }

        return that;
    }

    // Allow simple drawing of images for particles
    function drawImage(spec) {
		context.save();
		
		context.translate(spec.center.x, spec.center.y);
		context.rotate(spec.rotation);
		context.translate(-spec.center.x, -spec.center.y);
		
		context.drawImage(
			spec.image, 
			spec.center.x - spec.size/2, 
			spec.center.y - spec.size/2,
			spec.size, spec.size);
		
		context.restore();
	}

    // Create text function for rendering text
    function Text(spec) {
        var that = {};

        that.updateRotation = function(angle) {
            spec.rotation += angle;
        }

        function measureTextHeight(spec) {
            context.save();

            context.font = spec.font;
            context.fillStyle = spec.fill;
            context.strokeStyle = spec.stroke;

            var height = context.measureText('m').width;

            context.restore();

            return height;
        }

        function measureTextWidth(spec) {
            context.save();
            
            context.font = spec.font;
            context.fillStyle = spec.fill;
            context.strokeStyle = spec.stroke;

            var width = context.measureText(spec.text).width;

            context.restore();

            return width;
        }

        that.draw = function() {
            context.save();

            context.font = spec.font;
            context.fillStyle = spec.fill;
            context.strokeStyle = spec.stroke;
            context.textBaseline = 'top';
            context.textAlign = 'center';

            context.translate(spec.location.x + that.width/2, spec.location.y + that.height /2);
            context.rotate(spec.rotation);
            context.translate(-(spec.location.x + that.width/2), -(spec.location.y + that.height / 2));
            context.fillText(spec.text, spec.location.x, spec.location.y);
            context.strokeText(spec.text, spec.location.x, spec.location.y);

            context.restore();
        }

        // Compute public properties for the text
        that.height = measureTextHeight(spec);
        that.width = measureTextWidth(spec);
        that.location = spec.location;

        return that;
    }

    // Function to clear the canvas
    function clear() {
        context.clear();
    }

    function beginRender() {
        context.clear();
    }

    return {
        beginRender: beginRender,
        initialize: initialize,
        drawImage: drawImage,
        Text: Text,
        Texture: Texture,
        Rectangle: Rectangle,
        getCanvasWidth: getCanvasWidth,
        getCanvasHeight: getCanvasHeight
    }
}());
